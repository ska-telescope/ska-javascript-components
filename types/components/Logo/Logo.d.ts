import * as React from 'react';
export declare const LOGO_DEFAULT_HEIGHT = 30;
interface LogoProps {
    ariaDescription?: string;
    ariaTitle?: string;
    dark?: boolean;
    flatten?: boolean;
    height?: number;
}
export declare function Logo({ ariaDescription, ariaTitle, dark, flatten, height, }: LogoProps): React.JSX.Element;
export {};
