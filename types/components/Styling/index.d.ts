export declare const Styling: () => {
    BORDER_RADIUS: number;
    FONT_FAMILY: string;
    TRANSITION: number;
};
