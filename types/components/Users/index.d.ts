export type User = {
    username: string;
    email?: string;
    auth_aws?: {
        token?: string;
    };
};
export interface UserState {
    user: User | null;
}
