import * as React from 'react';
interface StatusProps {
    ariaDescription?: string;
    ariaTitle?: string;
    children?: React.ReactElement | string;
    level?: number;
    size?: number;
    testId: string;
    text?: string;
}
export declare function Status({ ariaDescription, ariaTitle, children, level, size, testId, text, }: StatusProps): React.JSX.Element;
export {};
