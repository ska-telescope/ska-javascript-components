export type Telescope = {
    code: string;
    name: string;
    location: string;
    position: {
        lat: number;
        lon: number;
    };
    image: string;
};
export interface TelescopeState {
    telescope: Telescope | null;
}
export declare const TELESCOPE_LOW: {
    code: string;
    name: string;
    location: string;
    position: {
        lat: number;
        lon: number;
    };
    image: string;
};
export declare const TELESCOPE_MID: {
    code: string;
    name: string;
    location: string;
    position: {
        lat: number;
        lon: number;
    };
    image: string;
};
export declare const TelescopeList: {
    code: string;
    name: string;
    location: string;
    position: {
        lat: number;
        lon: number;
    };
    image: string;
}[];
export interface TelescopeState {
    telescope: Telescope | null;
}
