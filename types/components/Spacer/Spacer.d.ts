import React from 'react';
export declare const SPACER_HORIZONTAL = "horizontal";
export declare const SPACER_VERTICAL = "vertical";
interface SpacerProps {
    size?: number;
    axis?: string;
}
export declare function Spacer({ size, axis }: SpacerProps): React.JSX.Element;
export {};
