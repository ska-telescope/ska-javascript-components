export type Help = {
    content: Object;
    component: Object;
    showHelp: Boolean;
};
