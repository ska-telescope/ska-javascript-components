import * as React from 'react';
export declare const SYMBOL_DEFAULT_HEIGHT = 30;
interface SymbolProps {
    ariaDescription?: string;
    ariaTitle?: string;
    dark?: boolean;
    flatten?: boolean;
    height?: number;
}
export declare function Symbol({ ariaDescription, ariaTitle, dark, flatten, height, }: SymbolProps): React.JSX.Element;
export {};
