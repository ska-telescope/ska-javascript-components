export declare const THEME_LIGHT = "light";
export declare const THEME_DARK = "dark";
export declare const Theme: (mode: string) => {
    components: {
        MuiAlert: {
            styleOverrides: {
                standardSuccess: {
                    backgroundColor: string;
                    color: string;
                };
                standardError: {
                    backgroundColor: string;
                    color: string;
                };
                standardWarning: {
                    backgroundColor: string;
                    color: string;
                };
                standardInfo: {
                    backgroundColor: string;
                    color: string;
                };
            };
        };
        MuiListItem: {
            styleOverrides: {
                root: {
                    backgroundColor: string;
                    '&.Mui-selected': {
                        backgroundColor: string;
                    };
                };
            };
        };
    };
    palette: {
        mode: string;
        primary: {
            light: string;
            main: string;
            dark: string;
            contrastText: string;
        };
        secondary: {
            light: string;
            main: string;
            dark: string;
            contrastText: string;
        };
    };
    shape: {
        borderRadius: number;
    };
    typography: {
        fontFamily: string;
    };
};
