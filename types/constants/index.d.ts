export declare const DARK_PRIMARY: string[];
export declare const DARK_SECONDARY: string[];
export declare const ERROR_1: string[];
export declare const ERROR_2: string[];
export declare const ERROR_3: string[];
export declare const ERROR_4: string[];
export declare const ERROR_5: string[];
export declare const LIGHT_PRIMARY: string[];
export declare const LIGHT_SECONDARY: string[];
export declare const SUCCESS: string[];
export type Help = {
    content: Object;
    component: Object;
    showHelp: Boolean;
};
export type User = {
    username: string;
    email?: string;
    auth_aws?: {
        token?: string;
    };
};
export interface UserState {
    user: User | null;
}
export type Telescope = {
    code: string;
    name: string;
    location: string;
    position: {
        lat: number;
        lon: number;
    };
    image: string;
};
export interface TelescopeState {
    telescope: Telescope | null;
}
