Theme
~~~~~

*Overview*

This is the latest SKAO Theme for use within all SKAO applications.
Also provided are standard constants that can be used for toggling between light and dark themes.

.. admonition:: *Code snippet* ./services/theme.tsx

   import { createTheme } from '@mui/material';
   import { Theme } from '@ska-telescope/ska-javascript-components';
   
   const theme = mode => createTheme(Theme(mode));
   export default theme;

.. admonition:: *Code snippet* .App.tsx
  import theme from '../services/theme/theme';

  function App() {
    const [apiVersion, setAPIVersion] = React.useState("LOCAL");
    const { t } = useTranslation('dpd');

    const skao = t('toolTip.button.skao');
    const mode = t('toolTip.button.mode');
    const toolTip = { skao: skao, mode: mode };

    // Theme related
    const [themeMode, setThemeMode] = React.useState(THEME_LIGHT);

    const themeToggle = () => {
    setThemeMode(themeMode === THEME_LIGHT ? THEME_DARK : THEME_LIGHT);

  };


    return (
      <ThemeProvider theme={theme(themeMode)}>
      <CssBaseline enableColorScheme />

        ...

      </ThemeProvider>

    );
    
  }

  export default App;

.. admonition:: Testing Identifier

   Testing of the theme will be done at component and page level as required
