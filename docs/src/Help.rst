Help
~~~~

*Overview*

A standard help type for use within the SKAO are provided here.

.. admonition:: Example

   import { Help } from '@ska-telescope/ska-javascript-components';

.. admonition:: Properties
   
    :header: "Property", "Type", "Required", "default", ""

    "content",  "object", "No", "null", "Intended use is that help that is relevant to the page/group is placed in here"
    "component","object", "No", "null", "Intended use is that help that is relevant to a simple component is placed in here"
    "showHelp","boolean", "No", "false", "display of the help is indicated by the status of this field"

.. admonition:: Constants
   
   None

.. admonition:: Testing Identifier

   Not applicable