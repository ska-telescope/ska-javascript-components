Spacer
~~~~~~

*Overview*

Component that allows for spacing to be added

*Image not applicable*

.. admonition:: Example

   import { SPACER_HORIZONTAL, SPACER_VERTICAL, Spacer } from '@ska-telescope/ska-javascript-components';


   <Spacer />

.. csv-table:: Properties
   :header: "Property", "Type", "Required", "default", ""

   "size","number", "No", "90", "Defines the size of the space in pixels"
   "axis","string", "No", "SPACER_HORIZONTAL", "SPACER_HORIZONTAL or SPACER_VERTICAL permitted"

.. admonition:: Constants

   - SPACER_HORIZONTAL
   - SPACER_VERTICAL

.. admonition:: Testing Identifier

   Currently there is no requirement to specifically test this component