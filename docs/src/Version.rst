Version
~~~~~~~

*Overview*

This is a simple constant that provides the latest version of the library

.. admonition:: *Code snippet* ./services/theme.tsx

   import { createTheme } from '@mui/material';

   
   import { JAVASCRIPT_COMPONENTS_VERSION } from '@ska-telescope/ska-javascript-components';
   
.. admonition:: Testing Identifier

   n/a