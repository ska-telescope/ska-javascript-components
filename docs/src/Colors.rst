Colors
~~~~~~

*Overview*

The standard colors in use within the SKAO are provided here.
It is expected that these will be used via the Theme, but have been provided for completeness

.. admonition:: Example

   import { Colors } from '@ska-telescope/ska-javascript-components';
   ...
   Colors().STATUS_BG_0

.. admonition:: Properties
   
   None

.. admonition:: Constants
   
   None

.. admonition:: Testing Identifier

   Testing of styling will be done on the component if it is required