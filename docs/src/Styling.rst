Styling
~~~~~~~

*Overview*

The standard styling in use within the SKAO are provided here.
It is expected that these will be used via the Theme, but have been provided for completeness

.. admonition:: Example

   import { Styling } from '@ska-telescope/ska-javascript-components';


   Styling().BORDER_RADIUS

.. admonition:: Properties
   
   None

.. admonition:: Constants

   None

.. admonition:: Testing Identifier

   Testing of styling will be done on the component if it is required