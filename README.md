# SKA Javascript Components

## Adding library to your application

See /docs/src/usage.rst
## Updating the CI/CD processor

See /docs/src/usage.rst

```

## Adding a component to the library

1. Add the component in the normal way; using storybook should help with this
2. Update the version number within package.json
3. Update the version number within the src/components/version/index.ts
4. Update the revision & version number within the docs/src/conf.py
5. Merge as usual, with a passing pipeline.
6. Once the merge has completed, the tag will need to be added in git.
   5a. Locate and select the 'Tags' submenu item
   5b. Click on New Tag, located towards the top-right of the page.
   5c. Enter the tag name in the form n.n.n ( eg. 0.1.2 )
   5d. Ensure that the 'Create from' is set to 'master'.
   5e. Enter an appropriate Message
   5f. Click 'Create tag'.

This will run the CI/CD process again, and once complete the executable will be associated by the version

## Functionality available within this library

See /docs/src/functionality.rst
