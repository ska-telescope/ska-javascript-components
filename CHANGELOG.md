Changelog
==========

2.2.4
*****

Update rollup-typescript and vite library versions

2.2.3
*****

Rebased from the version below

2.0.14
******

Re-attach the telescopes

2.0.13
******

Changed vite config

2.0.12
******

Changed so that index.ts is used

2.0.11
******

Issue with TelescopeList

2.0.10
******

Issue with TelescopeList

2.0.9
*****

Migrating to ES modules

2.0.8
*****

Final color adjusted to WCAG AAA
Replaced babel with SWC

2.0.7
*****

Colors checked to be WCAG AAA

2.0.6
*****

Add flatten

2.0.4
*****

Change the id of the the color shading for the Log & Symbol as they were clashing.

2.0.3
*****

Added Symbol to the library

pre-versioning 
**************

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).
