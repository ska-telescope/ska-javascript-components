import React from 'react';
import { Logo, LOGO_DEFAULT_HEIGHT } from './Logo';

const TEST_HEIGHT = 50;

describe('<Logo />', () => {
  it('Logo : light, default height', () => {
    cy.mount(<Logo dark={false} height={LOGO_DEFAULT_HEIGHT} />);
    cy.get('[data-testid="skaoLogo"]').invoke('height').should('equal', LOGO_DEFAULT_HEIGHT);
    cy.get('[data-testid="skaoLogo"]').should('be.visible').click();
  });

  it('Logo : Dark, default height ', () => {
    cy.mount(<Logo dark={true} height={LOGO_DEFAULT_HEIGHT} />);
    cy.get('[data-testid="skaoLogo"]').invoke('height').should('equal', LOGO_DEFAULT_HEIGHT);
    cy.get('[data-testid="skaoLogo"]').should('be.visible').click();
  });

  it('Logo : Light, Height = ' + TEST_HEIGHT, () => {
    cy.mount(<Logo dark={false} height={TEST_HEIGHT} />);
    cy.get('[data-testid="skaoLogo"]').invoke('height').should('equal', TEST_HEIGHT);
    cy.get('[data-testid="skaoLogo"]').should('be.visible').click();
  });

  it('Logo : Dark, Height = ' + TEST_HEIGHT, () => {
    cy.mount(<Logo dark={true} height={TEST_HEIGHT} />);
    cy.get('[data-testid="skaoLogo"]').invoke('height').should('equal', TEST_HEIGHT);
    cy.get('[data-testid="skaoLogo"]').should('be.visible').click();
  });

  it('Logo : light, flatten, default height', () => {
    cy.mount(<Logo dark={false} flatten height={LOGO_DEFAULT_HEIGHT} />);
    cy.get('[data-testid="skaoLogo"]').invoke('height').should('equal', LOGO_DEFAULT_HEIGHT);
    cy.get('[data-testid="skaoLogo"]').should('be.visible').click();
  });

  it('Logo : Dark, flatten, default height ', () => {
    cy.mount(<Logo dark={true} flatten height={LOGO_DEFAULT_HEIGHT} />);
    cy.get('[data-testid="skaoLogo"]').invoke('height').should('equal', LOGO_DEFAULT_HEIGHT);
    cy.get('[data-testid="skaoLogo"]').should('be.visible').click();
  });

  it('Logo : Light, flatten, Height = ' + TEST_HEIGHT, () => {
    cy.mount(<Logo dark={false} flatten height={TEST_HEIGHT} />);
    cy.get('[data-testid="skaoLogo"]').invoke('height').should('equal', TEST_HEIGHT);
    cy.get('[data-testid="skaoLogo"]').should('be.visible').click();
  });

  it('Logo : Dark, flatten, Height = ' + TEST_HEIGHT, () => {
    cy.mount(<Logo dark={true} flatten height={TEST_HEIGHT} />);
    cy.get('[data-testid="skaoLogo"]').invoke('height').should('equal', TEST_HEIGHT);
    cy.get('[data-testid="skaoLogo"]').should('be.visible').click();
  });
});
