import React from 'react';
import { Status } from './Status';

const stats = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
const TEST_SIZE = 50;
const DEFAULT_SIZE = 60;

describe('<Status />', () => {
  it('Status default renders', () => {
    cy.mount(<Status testId="statusTestId" />);
    cy.get('[data-testid="statusTestId"]').should('be.visible');
    cy.get('[data-testid="statusTestId"]').invoke('height').should('equal', DEFAULT_SIZE);
  });

  for (const i of stats) {
    it('Status ' + i + ' renders', () => {
      cy.mount(
        <Status testId="statusTestId" level={i}>
          {i.toString()}
        </Status>,
      );
      cy.get('[data-testid="statusTestId"]').should('be.visible');
      cy.get('[data-testid="statusTestId"]').invoke('height').should('equal', DEFAULT_SIZE);
      cy.get('[data-testid="statusTestId"]').should('contain.text', i.toString());
    });
    it('Status ' + i + ' renders with NO text', () => {
      cy.mount(
        <Status
          ariaDescription="Test ARIA description"
          ariaTitle="Test ARIA title"
          level={i}
          size={TEST_SIZE}
          testId="statusTestId"
        >
          {i.toString()}
        </Status>,
      );
      cy.get('[data-testid="statusTestId"]').should('be.visible');
      cy.get('[data-testid="statusTestId"]').invoke('height').should('equal', TEST_SIZE);
      cy.get('[data-testid="statusTestId"]').should('contain.text', i.toString());
    });
    it('Status ' + i + ' renders with text', () => {
      cy.mount(
        <Status
          ariaDescription="Test ARIA description"
          ariaTitle="Test ARIA title"
          level={i}
          testId="statusTestId"
          text={i.toString()}
        >
          {i.toString()}
        </Status>,
      );
      cy.get('[data-testid="statusTestId"]').should('be.visible');
      cy.get('[data-testid="statusTestId"]').invoke('height').should('equal', DEFAULT_SIZE);
      cy.get('[data-testid="statusTestId"]').children().should('contain.text', i.toString());
    });
  }
});
