import { Status } from './Status';

export default {
  title: 'Example/Status',
  component: Status,
  parameters: {
    layout: 'centered',
  },
};

export const Default = {
  args: {
    testId: 'statusTestId',
    level: 1,
  },
};
