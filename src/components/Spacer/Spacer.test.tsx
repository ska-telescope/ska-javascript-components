import React from 'react';
import { Spacer, SPACER_HORIZONTAL, SPACER_VERTICAL } from './Spacer';

describe('<Spacer />', () => {
  it('Spacer default renders', () => {
    cy.mount(<Spacer />);
  });
  it('Spacer horizontal axis renders', () => {
    cy.mount(<Spacer axis={SPACER_HORIZONTAL} />);
  });
  it('Spacer vertical axis renders', () => {
    cy.mount(<Spacer axis={SPACER_VERTICAL} />);
  });
});
