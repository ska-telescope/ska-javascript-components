import React from 'react';
import { Symbol, SYMBOL_DEFAULT_HEIGHT } from './Symbol';

const TEST_HEIGHT = 50;

describe('<Symbol />', () => {
  it('Symbol : light, default height', () => {
    cy.mount(<Symbol dark={false} height={SYMBOL_DEFAULT_HEIGHT} />);
    cy.get('[data-testid="skaoSymbol"]').invoke('height').should('equal', SYMBOL_DEFAULT_HEIGHT);
    cy.get('[data-testid="skaoSymbol"]').should('be.visible').click();
  });

  it('Symbol : Dark, default height ', () => {
    cy.mount(<Symbol dark={true} height={SYMBOL_DEFAULT_HEIGHT} />);
    cy.get('[data-testid="skaoSymbol"]').invoke('height').should('equal', SYMBOL_DEFAULT_HEIGHT);
    cy.get('[data-testid="skaoSymbol"]').should('be.visible').click();
  });

  it('Symbol : Light, Height = ' + TEST_HEIGHT, () => {
    cy.mount(<Symbol dark={false} height={TEST_HEIGHT} />);
    cy.get('[data-testid="skaoSymbol"]').invoke('height').should('equal', TEST_HEIGHT);
    cy.get('[data-testid="skaoSymbol"]').should('be.visible').click();
  });

  it('Symbol : Dark, Height = ' + TEST_HEIGHT, () => {
    cy.mount(<Symbol dark={true} height={TEST_HEIGHT} />);
    cy.get('[data-testid="skaoSymbol"]').invoke('height').should('equal', TEST_HEIGHT);
    cy.get('[data-testid="skaoSymbol"]').should('be.visible').click();
  });

  it('Symbol : light, flatten, default height', () => {
    cy.mount(<Symbol dark={false} flatten height={SYMBOL_DEFAULT_HEIGHT} />);
    cy.get('[data-testid="skaoSymbol"]').invoke('height').should('equal', SYMBOL_DEFAULT_HEIGHT);
    cy.get('[data-testid="skaoSymbol"]').should('be.visible').click();
  });

  it('Symbol : Dark, flatten, default height ', () => {
    cy.mount(<Symbol dark={true} flatten height={SYMBOL_DEFAULT_HEIGHT} />);
    cy.get('[data-testid="skaoSymbol"]').invoke('height').should('equal', SYMBOL_DEFAULT_HEIGHT);
    cy.get('[data-testid="skaoSymbol"]').should('be.visible').click();
  });

  it('Symbol : Light, flatten, Height = ' + TEST_HEIGHT, () => {
    cy.mount(<Symbol dark={false} flatten height={TEST_HEIGHT} />);
    cy.get('[data-testid="skaoSymbol"]').invoke('height').should('equal', TEST_HEIGHT);
    cy.get('[data-testid="skaoSymbol"]').should('be.visible').click();
  });

  it('Symbol : Dark, flatten, Height = ' + TEST_HEIGHT, () => {
    cy.mount(<Symbol dark={true} flatten height={TEST_HEIGHT} />);
    cy.get('[data-testid="skaoSymbol"]').invoke('height').should('equal', TEST_HEIGHT);
    cy.get('[data-testid="skaoSymbol"]').should('be.visible').click();
  });
});
