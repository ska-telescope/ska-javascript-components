import { Colors } from '../Colors/index.ts';
import { Styling } from '../Styling/index.js';

export const THEME_LIGHT = 'light';
export const THEME_DARK = 'dark';

export const Theme = (mode: string) => {
  return {
    components: {
      MuiAlert: {
        styleOverrides: {
          standardSuccess: {
            backgroundColor: Colors().STATUS_BG_0,
            color: Colors().STATUS_FG_0,
          },
          standardError: {
            backgroundColor: Colors().STATUS_BG_1,
            color: Colors().STATUS_FG_1,
          },
          standardWarning: {
            backgroundColor: Colors().STATUS_BG_3,
            color: Colors().STATUS_FG_3,
          },
          standardInfo: {
            backgroundColor: Colors().STATUS_BG_5,
            color: Colors().STATUS_FG_5,
          },
        },
      },
      MuiListItem: {
        styleOverrides: {
          root: {
            backgroundColor: 'blue',

            '&.Mui-selected': {
              backgroundColor: 'secondary.main',
            },
          },
        },
      },
    },
    palette: {
      mode,
      primary: {
        light: Colors().LIGHT_PRIMARY_LIGHT,
        main: Colors().LIGHT_PRIMARY_MAIN,
        dark: Colors().LIGHT_PRIMARY_DARK,
        contrastText: Colors().LIGHT_PRIMARY_CONTRAST,
        ...(mode === 'dark' && {
          light: Colors().DARK_PRIMARY_LIGHT,
          main: Colors().DARK_PRIMARY_MAIN,
          dark: Colors().DARK_PRIMARY_DARK,
          contrastText: Colors().DARK_PRIMARY_CONTRAST,
        }),
      },
      secondary: {
        light: Colors().LIGHT_SECONDARY_LIGHT,
        main: Colors().LIGHT_SECONDARY_MAIN,
        dark: Colors().LIGHT_SECONDARY_DARK,
        contrastText: Colors().LIGHT_SECONDARY_CONTRAST,
        ...(mode === 'dark' && {
          light: Colors().DARK_SECONDARY_LIGHT,
          main: Colors().DARK_SECONDARY_MAIN,
          dark: Colors().DARK_SECONDARY_DARK,
          contrastText: Colors().DARK_SECONDARY_CONTRAST,
        }),
      },
    },
    shape: {
      borderRadius: Styling().BORDER_RADIUS,
    },
    typography: {
      fontFamily: Styling().FONT_FAMILY,
    },
  };
};
