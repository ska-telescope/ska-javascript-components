export const Styling = () => ({
  BORDER_RADIUS: 8,
  FONT_FAMILY: 'Helvetica, Arial, sans-serif',
  TRANSITION: 0.5,
});
