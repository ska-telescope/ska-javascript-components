export const DARK_PRIMARY = ['#484848', '#212121', '#888888', '#FFFFFF', '#FFFFFF', '#FFFFFF'];
export const DARK_SECONDARY = ['#FFF04E', '#F7BE00', '#BF8E00', '#000000', '#000000', '#000000'];

export const ERROR_1 = ['#FF5F52', '#AD1F1F', '#8A0000', '#000000', '#FFFFFF', '#FFFFFF'];
export const ERROR_2 = ['#FFFF6B', '#ffe939', '#C6A700', '#000000', '#000000', '#000000'];
export const ERROR_3 = ['#FFBD45', '#f6a45a', '#C25E00', '#000000', '#000000', '#000000'];
export const ERROR_4 = ['#FF6090', '#87c3eb', '#B0003A', '#000000', '#000000', '#FFFFFF'];
export const ERROR_5 = ['#FFFFFF', '#FFFFFF', '#C2C2C2', '#000000', '#000000', '#000000'];

export const LIGHT_PRIMARY = ['#C1C6CA', '#F4F9FD', '#919599', '#000000', '#000000', '#000000'];
export const LIGHT_SECONDARY = ['#472A97', '#070068', '#00003D', '#FFFFFF', '#FFFFFF', '#FFFFFF'];

export const SUCCESS = ['#6ABF69', '#015b00', '#00600F', '#000000', '#FFFFFF', '#FFFFFF'];

export type Help = {
  content: Object;
  component: Object;
  showHelp: Boolean;
};

export type User = {
  username: string;
  email?: string;
  auth_aws?: {
    token?: string;
  };
};

export interface UserState {
  user: User | null;
}

export type Telescope = {
  code: string;
  name: string;
  location: string;
  position: {
    lat: number;
    lon: number;
  };
  image: string;
};

export interface TelescopeState {
  telescope: Telescope | null;
}
